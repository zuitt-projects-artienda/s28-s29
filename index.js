// Express js
// First, we load the expressjs module into our application and save it in a variable called express
const express = require('express');

//Create an application with expressjs. This creates an application that uses express and stores it as app.
//app is our server.
const app = express();

// port is a variable that contains the port number we want to designate to our server
const port = 4000;

//middleware
//express.json()- a method from express which allow us to handle the streaming of data and automatically parse the incoming JSON from our req body.
//app.use is used to run a method or another function for our expressjs api
app.use(express.json());

// mock data
let users = [
	{
		username: "BMadrigal",
		email: "fateReader@gmail.com",
		password: "weDontTalkAboutMe"
	},
	{
		username: "LuisaMadrigal",
		email: "strongSis@gmail.com",
		password: "pressure"
	}
];

let items = [
	{
		name: "roses",
		price: 170,
		isActive: true
	},
	{
		name: "tulips",
		price: 250,
		isActive: true
	}
];

//Express has a methods to use as routes corresponding to HTTP methods.
//app.get(<endpoint>, <function handling req and res>)
app.get("/", (req, res) =>{
	//Once the route is accessed, we can send a response with the use of res.send()
	//res.send() actually combines writeHead() and end().
	res.send("Hello from ExpressJS Api!")
});

app.get("/greeting", (req, res) =>{

	res.send("Hello from Bacth169-artienda")
});

app.get("/users", (req, res) => {
	res.send(users);
})

//get data from client
app.post("/users", (req, res) => {

	console.log(req.body)

	let reqBody = req.body

	let newUser = {
		username: reqBody.username,
		email: reqBody.email,
		password: reqBody.password
	}
	users.push(newUser);
	console.log(users);
	res.send(users);
})

app.delete("/users", (req, res) =>{
	users.pop();
	console.log(users);
	res.send(users);
})

//Updating users
//:index is a wildcard for specifity
//when connected in the database, we use ID
app.put("/users/:index", (req, res) => {
	//req.body - this will contain the updated password
	console.log(req.body);

	//req.params object which contains the value in the url params
	//url params is captured by route parameter (:parameterName) and saved as property in req.params
	console.log(req.params);

	//parseInt the value of the number coming from req.params
	let index = parseInt(req.params.index);

	//get the user that we want to update with our index number from the url params
	users[index].password = req.body.password

	//sends the updated password to client
	res.send(users[index]);
})

//getting a specific user using params
app.get("/users/getSingleUser/:index" , (req, res) => {
	let index = parseInt(req.params.index);
	res.send(users[index]);
})

//ACTIVITY-------

app.get("/items", (req, res) => {
	res.send(items);
});

app.post("/items", (req, res) => {

	console.log(req.body)

	let reqBody = req.body

	let newItem = {
		name: reqBody.name,
		price: reqBody.price,
		isActive: reqBody.isActive
	}
	items.push(newItem);
	console.log(items);
	res.send(items);
});

app.put("/items/:index", (req, res) => {

	let index = parseInt(req.params.index);

	items[index].price = req.body.price

	res.send(items[index]);
});


// A C T I V I T Y

app.get("/items/getSingleUser/:index" , (req, res) => {
	let index = parseInt(req.params.index);
	res.send(items[index]);
});

app.put("/items/archive/:index", (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = req.body.isActive

	res.send(items[index]);
});

app.put("/items/activate/:index", (req, res) => {

	let index = parseInt(req.params.index);

	items[index].isActive = req.body.isActive

	res.send(items[index]);
});

app.listen(port, () => console.log(`Server is running at port ${port}`))